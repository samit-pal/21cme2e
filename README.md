# 21cm End-to-End (21cmE2E) Pipeline

The **21cmE2E** pipeline is a Python package designed to simulate realistic radio observations of the Cosmic Dawn/Epoch of Reionization by incorporating various instrumental effects and signal corruptions. Mazumder et al. 2022 have developed the 21cmE2E pipeline to simulate interferometric observation.

## It includes:
* Gain calibration errors. 
* Position shift errors.
* Time- and direction-dependent ionospheric effects.
* Station Beam effects.
* Telescopic noise.

## Description of the pipeline
We have designed an end-to-end simulation pipeline for mock interferometric observations at low frequencies to quantify and understand systematic effects such as calibration errors, position errors, and ionospheric errors.In the current version, our pipeline integrates the Common Astronomy Software Applications ([CASA](https://casa.nrao.edu/)) simulation tool [McMullin et al., 2007](https://adsabs.harvard.edu/full/2007ASPC..376..127M) with the [OSKAR](https://ska-telescope.gitlab.io/sim/oskar/) software package [Dulwich et al., 2009](https://ui.adsabs.harvard.edu/link_gateway/2009wska.confE..31D/doi:10.22323/1.132.0031). CASA offers comprehensive functionalities for simulating radio astronomical observations. It supports various telescope configurations, allowing users to define array layouts, frequency bands, observing times, and telescope beam patterns,thermal noise, and more. OSKAR is another simulation tool designed to generate interferometric visibilities for aperture arrays. It allows for the simulation of the radio sky using source catalogs or FITS files, and supports array and array element layout files for aperture arrays, such as SKA-Low. It also includes various beam patterns and noise characteristics. By combining CASA and OSKAR, our pipeline can produce mock observations for both dish telescopes and aperture arrays. For more details about this pipeline, see [Mazumder et al. 2022](https://doi.org/10.1007/s12036-022-09906-8).

## Flowchart of the End-to-End pipeline
Here is a step-by-step description of the basic operation of the end-to-end simulation pipeline:
![img.png](documentation/images/pipeline.png)

